import React from 'react';
import ReactDOM from 'react-dom';
import Home from './views/Home/Home';
import MaisVotadas from './views/MaisVotadas/MaisVotadas';
import './index.css';
import * as serviceWorker from './serviceWorker';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { makeStyles } from '@material-ui/core/styles';
//Material-UI Imports
import { Grid, Typography, Container, Button } from '@material-ui/core';
import { Link as LinkMUI } from '@material-ui/core';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    body: {
        background: 'rgba(0, 0, 0, 0.05)',
        fontFamily: 'Helvetica'
    },
    header: {
        background: 'radial-gradient(circle, rgba(198,198,198,1) 0%, rgba(53,131,181,1) 100%)',
        fontFamily: 'Helvetica'
    },
    footer: {
        background: 'radial-gradient(circle, rgba(198,198,198,1) 0%, rgba(53,131,181,1) 100%)',
        padding: '1vmin 0 1vmin',
        margin: '3vmin 0 0',
        textAlign: 'center'
    },
    titulo: {
        padding: '3vmin 0 4vmin'
    },
    buttons: {
        padding: '0 0 2vmin'
    }
}));

const client = new ApolloClient({
    uri: 'https://startups-project-mytvsxrgeb.now.sh'
});

function App() {
    const classes = useStyles();

    return (
        <Router>
            <div className={classes.header}>
                <header>
                    <Container>
                        <Typography className={classes.titulo} component="h1" variant="h3" align="center" color="primary">
                            #1 Startup Fest
                        </Typography>
                        <Grid container spacing={4} justify="center" className={classes.buttons}>
                            <Grid item>
                                <Link to="/">
                                    <Button variant="outlined" color="primary">
                                        Startups
                                    </Button>
                                </Link>
                            </Grid>
                            <Grid item>
                                <Link to="/maisvotadas">
                                    <Button variant="outlined" color="primary">
                                        Mais Votadas
                                    </Button>
                                </Link>
                            </Grid>
                        </Grid>
                    </Container>
                </header>
            </div>
            <div className={classes.body}>
                <ApolloProvider client={client}>
                    <main>
                        <Switch>
                            <Route path="/maisvotadas">
                                <MaisVotadas />
                            </Route>
                            <Route path="/">
                                <Home />
                            </Route>
                        </Switch>
                    </main>
                </ApolloProvider>
                <footer className={classes.footer}>
                    <Container>
                        <Typography variant="caption" component="p" color="primary">
                        <LinkMUI href={"https://www.linkedin.com/in/leandro-gasparim-778987193/"}>Desenvolvido por Leandro Gasparim</LinkMUI>
                        </Typography>
                    </Container>
                </footer>
            </div>
        </Router>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.unregister();
