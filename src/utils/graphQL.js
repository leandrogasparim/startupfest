import { gql } from "apollo-boost";

export const ALLSTARTUPS_QUERY = gql`
  query {
    allStartups {
      name
      imageUrl
      description
      Segment {
        name
      }
    }
  }
`;