
import { firebaseDB } from '../utils/firebase';
import { sortByAttribute } from './misc';

export function SetRating(empresa, categoria, rating) {
    firebaseDB.collection("ratings").doc(empresa).get().then(doc => {
        let data = doc.data();

        if (data !== undefined && data[categoria] !== undefined) {
            data[categoria].push(rating);
        } else {
            data = {
                [categoria]: [rating]
            }
        }

        firebaseDB.collection("ratings").doc(empresa).set(data, { merge: true }).then(function () {
            console.log("Voto Salvo.");
        }).catch(function (error) {
            console.error("Erro ao votar: ", error);
        });
    });
}

export async function GetRating(empresa, categoria) {
    let rating = 0;

    await firebaseDB.collection('ratings').doc(empresa).get().then((doc) => {
        let data = doc.data();

        if (data !== undefined && data[categoria] !== undefined) {
            let sum = data[categoria].reduce((accum, curr) => accum + curr);
            rating = sum / data[categoria].length;
        }
    });

    return rating;
}

export async function GetMostRated(categoria) {
    let newData = [];

    await firebaseDB.collection('ratings').get().then((docs) => {
        docs.forEach(doc => {
            let data = doc.data();
            if (data !== undefined && data[categoria] !== undefined) {
                let sum = data[categoria].reduce((accum, curr) => accum + curr);
                let rating = sum / data[categoria].length;

                newData.push({ empresa: doc.id, [categoria]: rating });
            }
        });

        newData = sortByAttribute(newData, "-"+categoria);
    });

   
    return newData;
}