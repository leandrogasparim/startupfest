
//Função para orgazinar array de objetos dado um atributo
//https://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value
export function sortByAttribute(array, ...attrs) {
    // generate an array of predicate-objects contains
    // property getter, and descending indicator
    let predicates = attrs.map(pred => {
        let descending = pred.charAt(0) === '-' ? -1 : 1;
        pred = pred.replace(/^-/, '');
        return {
            getter: o => o[pred],
            descend: descending
        };
    });
    // schwartzian transform idiom implementation. aka: "decorate-sort-undecorate"
    return array.map(item => {
        return {
            src: item,
            compareValues: predicates.map(predicate => predicate.getter(item))
        };
    })
        .sort((o1, o2) => {
            let i = -1, result = 0;
            while (++i < predicates.length) {
                if (o1.compareValues[i] < o2.compareValues[i]) result = -1;
                if (o1.compareValues[i] > o2.compareValues[i]) result = 1;
                if (result *= predicates[i].descend) break;
            }
            return result;
        })
        .map(item => item.src);
}

//Categorias para os votos
export const Categorias = [
    "Proposta", "Desenvolvimento", "Apresentação / Pitch"
];

export const Tooltips = {
    [Categorias[0]]: "A ideia / proposta agradou o ouvinte e teve um bom impacto",
    [Categorias[1]]: "A startup soube demonstrar a sua proposta",
    [Categorias[2]]: "No estagio atual do produto / serviço, atende bem a proposta?"
};