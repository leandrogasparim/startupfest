import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyARdPwO_yy02ZGFoFj4XNdPYCf-tD9qZRs",
    authDomain: "startup-fest-930b6.firebaseapp.com",
    databaseURL: "https://startup-fest-930b6.firebaseio.com",
    projectId: "startup-fest-930b6",
    storageBucket: "startup-fest-930b6.appspot.com",
    messagingSenderId: "642651954606",
    appId: "1:642651954606:web:3fb105462a60d519267c3f"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export const firebaseDB = firebase.firestore();
