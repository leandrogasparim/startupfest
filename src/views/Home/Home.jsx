import React from 'react';
import AllStartups from '../../components/AllStartups/AllStartups';
//Material-UI Imports
import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    startups: {
        padding: '4vmin 0 0'
    }
}));

export default function Home() {
    const classes = useStyles();

    return (
        <div className={classes.startups}>
            <Container>
                <AllStartups />
            </Container>
        </div>
    );
}