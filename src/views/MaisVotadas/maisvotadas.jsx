import React from 'react';
import TopRatings from '../../components/TopRatings/TopRatings';
import { Categorias, Tooltips } from '../../utils/misc';

//Material-UI Imports
import {
    Typography, Container, Tooltip
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    startups: {
        padding: '4vmin 0 0'
    },
    categoria: {
        margin: '3vmin 0 2vmin'
    }
}));

export default function MaisVotadas() {
    const classes = useStyles();

    return (
        <div className={classes.startups}>
            <Container maxWidth="sm">
                {Categorias.map((categoria, id) => (
                    <div key={id}>
                        <Tooltip title={Tooltips[categoria]} placement="top-start">
                            <Typography key={categoria} gutterBottom variant="h4" component="p" className={classes.categoria} color="primary">
                                {categoria}
                            </Typography>
                        </Tooltip>
                        <TopRatings categoria={categoria} />
                    </div>
                ))}
            </Container>
        </div>
    );
}