import React, { Component } from 'react'
import Rating from '@material-ui/lab/Rating';
import { Grid, Box } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { SetRating, GetRating } from '../../utils/firestore';

const StyledRating = withStyles({
    iconFilled: {
        color: 'black',
    },
    iconHover: {
        color: 'black',
    },
})(Rating);

export default class RatingStar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        };
        this.handleRatingClick = this.handleRatingClick.bind(this);
    }

    async componentDidMount() {
        let rating = await GetRating(this.props.empresa, this.props.title);
        this.setState({ value: rating });
    }

    handleRatingClick(empresa, categoria, rating) {
        //console.log(empresa + " " + categoria + " " + rating);
        SetRating(empresa, categoria, rating);
    }

    render() {
        return (
            <div>
                <Grid container direction="row" alignItems="center">
                    <Grid item>
                        <StyledRating
                            readOnly={this.props.readOnly}
                            name={this.props.title}
                            size="large"
                            value={this.props.readOnly ? this.props.value : this.state.value}
                            precision={0.5}
                            onChange={(event, value) => this.handleRatingClick(this.props.empresa, this.props.title, value)}
                        />
                    </Grid>
                    <Grid item>
                        {this.props.readOnly &&
                            <Box ml={2}>{parseFloat(this.state.value).toFixed(1)}/5</Box>
                        }
                    </Grid>
                </Grid>
            </div>
        );
    }
}