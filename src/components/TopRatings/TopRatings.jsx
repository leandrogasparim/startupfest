import React, { useEffect, useState } from 'react';
import { GetMostRated } from '../../utils/firestore';
import { useQuery } from '@apollo/react-hooks';
import { ALLSTARTUPS_QUERY } from '../../utils/graphQL';
import RatingStar from '../RatingStar/RatingStar';

//Material-UI Imports
import {
    CardMedia, Typography, CardContent, Card, Grid
} from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    card: {
        display: 'flex',
    },
    content: {
        display: 'flex',
    },
    media: {
        width: 150,
    },
}));

async function GetDataRatings(categoria) {
    let data = await GetMostRated(categoria);
    return data;
}

function GetEmpresaByName(empresas, name) {
    let empresa = {};
    for (let i = 0; i < empresas.length; i++) {
        let tmp_empresa = empresas[i];
        if (tmp_empresa.name === name) {
            empresa = tmp_empresa;
            break;
        }
    }

    return empresa;
}

function Loading() {
    return (
        <Grid container spacing={4} direction="column">
            <Grid item>
                <Skeleton variant="rect" height={100} />
            </Grid>
            <Grid item>
                <Skeleton variant="rect" height={100} />
            </Grid>
            <Grid item>
                <Skeleton variant="rect" height={100} />
            </Grid>
        </Grid>
    );
}

export default function TopRatings(props) {
    const { loading, error, data } = useQuery(ALLSTARTUPS_QUERY);
    const [dataRating, setDataRating] = useState([]);
    const classes = useStyles();

    useEffect(() => {
        const fetchData = async () => {
            const data = await GetDataRatings(props.categoria);
            setDataRating(data);
        };

        fetchData();
    }, [props.categoria]);

    if (loading) return <Loading />;
    if (error) return console.log(error);

    //console.log("startups", data);
    //console.log("dataRatings", dataRating);

    let components = [];

    for (let i = 0; i < dataRating.length; i++) {
        let dRating = dataRating[i];
        let empresa = GetEmpresaByName(data.allStartups, dRating.empresa);
        let valRating = dRating[props.categoria];

        components.push(
            <Grid key={i} item xs={12}>
                <Grid container alignItems="center">
                    <Grid item xs={1}>
                        <Typography gutterBottom variant="h6" component="p">
                            {(i + 1) + "º"}
                        </Typography>
                    </Grid>
                    <Grid item xs={11}>
                        <Card className={classes.card}>
                            <CardMedia
                                className={classes.media}
                                image={empresa.imageUrl}
                                title="Image"
                            />
                            <CardContent>
                                <Grid container direction="column">
                                    <Grid item>
                                        <Typography gutterBottom variant="h6" color="secondary">
                                            {empresa.name}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography gutterBottom variant="body2">
                                            {empresa.Segment.name}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <RatingStar title={props.categoria} empresa={empresa.name} readOnly={true} value={valRating} />
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </Grid>
        );
    }

    return (
        <div>
            <Grid container spacing={2}>
                {components}
            </Grid>
        </div>
    );
}