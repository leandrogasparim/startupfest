import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { ALLSTARTUPS_QUERY } from '../../utils/graphQL';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import RatingStar from '../RatingStar/RatingStar';
import { Categorias, Tooltips } from '../../utils/misc';

//Material-UI Imports
import {
  CardMedia, Typography, CardContent, CardActions, Card, Grid, Container, Collapse,
  IconButton, Tooltip
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 350,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  ratingStar: {
    marginTop: '4vmin'
  }
}));

function Loading() {
  return (
    <Grid container spacing={4}>
      <Grid item xs={12} sm={6} md={4}>
        <Skeleton variant="rect" height={250} />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <Skeleton variant="rect" height={250} />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <Skeleton variant="rect" height={250} />
      </Grid>
    </Grid>
  );
}

export default function AllStartups() {
  const { loading, error, data } = useQuery(ALLSTARTUPS_QUERY);
  const classes = useStyles();
  const [expanded, setExpanded] = useState([]);
  const [lastExpanded, setLastExpanded] = useState(undefined);

  if (loading) return <Loading />;
  if (error) return console.log(error);

  if (expanded.length <= 0) {
    for (let i = 0; i < data.allStartups.length; i++) {
      expanded.push(false);
    }
  }

  const handleExpandClick = (id) => {
    let tmp_expanded = [...expanded];
    tmp_expanded[id] = !tmp_expanded[id];

    if (lastExpanded !== undefined && lastExpanded !== id)
      tmp_expanded[lastExpanded] = false;

    setExpanded(tmp_expanded);
    setLastExpanded(id);
  };

  return (
    <Container>
      <Grid container spacing={4}>
        {data.allStartups.map((startup, id) => (
          <Grid item key={id} xs={12} sm={6} md={4}>
            <Card className={classes.card} elevation={4}>
              <CardMedia
                className={classes.media}
                image={startup.imageUrl}
                title="Image"
              />

              <CardContent>
                <Grid container justify="center" alignItems="center">
                  <Typography gutterBottom variant="h5" component="h5">
                    {startup.name}
                  </Typography>
                </Grid>
                <Grid container justify="center" alignItems="center">
                  <Typography gutterBottom variant="body2" component="p">
                    {startup.Segment.name}
                  </Typography>
                </Grid>
              </CardContent>

              <CardActions disableSpacing>
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded[id],
                  })}
                  onClick={() => handleExpandClick(id)}
                  aria-expanded={expanded[id]}
                  aria-label="mais..."
                >
                  <ExpandMoreIcon />
                </IconButton>
              </CardActions>
              <Collapse in={expanded[id]} timeout="auto" unmountOnExit>
                <CardContent>
                  <Container>
                    <Typography variant="body2" color="textSecondary" component="p">
                      {startup.description}
                    </Typography>
                  </Container>
                  {Categorias.map((categoria, id) => (
                    <Grid key={categoria} container justify="center" alignItems="center" className={classes.ratingStar}>
                      <Tooltip title={Tooltips[categoria]} placement="top">
                        <Grid container justify="center" alignItems="center">
                          <Typography component="legend" color="primary">{categoria}</Typography>
                        </Grid>
                      </Tooltip>
                      <RatingStar title={categoria} empresa={startup.name} readOnly={false} />
                    </Grid>
                  ))}
                </CardContent>
              </Collapse>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container >
  );
}
